# README #

A repoban talalhato egy kis halozattudomanyos projekt. Az MSc alatt ezzel foglalkozok a legtobbet.

### Mirol is van szo ###

Egy absztrakt halozatmodellt implementaltam a kiadott tudomanyos cikk alapjan, majd kulonbozo mereseket vegeztem rajta,
illetve ebbol irtam egy rovid osszefoglalot (amit most nem tettem fel, mert az nem resze a kodnak).

Forras: [cikk](https://www.nature.com/articles/nature11459)